import pytest
from django.urls.base import reverse

from ufollower.apps.subscriptions.models import MyComment
from ufollower.apps.subscriptions.models import MyNewEntry
from ufollower.apps.subscriptions.models import MySavedEntry
from ufollower.apps.subscriptions.models import MyStarredEntry

from ..factories import EntryFactory
from ..factories import FeedFactory
from ..factories import MyCommentFactory
from ..factories import MyFeedFactory
from ..factories import MyNewEntryFactory
from ..factories import MySavedEntryFactory
from ..factories import MyStarredEntryFactory


class TestEntriesList:
    @pytest.mark.django_db
    def test_success_no_login(self, api_client):
        EntryFactory.create_batch(5)
        response = api_client.get(reverse('api:entry-list'))
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_no_feeds_no_login(self, api_client):
        response = api_client.get(reverse('api:entry-list'))
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_success_with_login(self, user_client):
        EntryFactory.create_batch(5)
        response = user_client.get(reverse('api:entry-list'))
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_success_no_feeds_with_login(self, user_client):
        response = user_client.get(reverse('api:entry-list'))
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_success_with_query(self, api_client):
        feed_1 = FeedFactory.create()
        feed_2 = FeedFactory.create()
        EntryFactory.create_batch(5, feed=feed_1)
        EntryFactory.create_batch(5, feed=feed_2)
        response = api_client.get(
            f'{reverse("api:entry-list")}?feed_id={feed_1.id}'
        )
        assert response.status_code == 200
        assert len(response.data) == 5


class TestEntryDetail:
    @pytest.mark.django_db
    def test_success_no_login(self, api_client):
        entry = EntryFactory.create()
        response = api_client.get(
            reverse('api:entry-detail', kwargs={'pk': entry.id})
        )
        assert response.status_code == 200
        assert response.data['id'] == entry.id

    @pytest.mark.django_db
    def test_success_with_login(self, user_client):
        entry = EntryFactory.create()
        response = user_client.get(
            reverse('api:entry-detail', kwargs={'pk': entry.id})
        )
        assert response.status_code == 200
        assert response.data['id'] == entry.id

    @pytest.mark.django_db
    def test_read_new_entry(self, user_client, existed_user):
        entry = EntryFactory.create()
        MyNewEntryFactory.create(user=existed_user, entry=entry)
        assert MyNewEntry.objects.count() == 1
        user_client.get(
            reverse('api:entry-detail', kwargs={'pk': entry.id})
        )
        assert MyNewEntry.objects.count() == 0


class TestEntryCommentsList:

    @pytest.mark.django_db
    def test_comments_list_no_login(self, api_client):
        entry = EntryFactory.create()
        MyCommentFactory.create_batch(5, entry=entry)
        response = api_client.get(
            reverse('api:entry-comment-list', kwargs={'pk': entry.id})
        )
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_comments_list_with_login(self, user_client):
        entry = EntryFactory.create()
        MyCommentFactory.create_batch(5, entry=entry)
        response = user_client.get(
            reverse('api:entry-comment-list', kwargs={'pk': entry.id})
        )
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_no_comments(self, api_client):
        entry = EntryFactory.create()
        response = api_client.get(
            reverse('api:entry-comment-list', kwargs={'pk': entry.id})
        )
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_entries_list_comments_field(self, api_client):
        entry = EntryFactory.create()
        MyCommentFactory.create_batch(5, entry=entry)
        response = api_client.get(reverse('api:entry-list'))
        assert response.status_code == 200
        assert response.data[0]['comments'] == 5


class TestEntryCommentAdd:

    @pytest.mark.django_db
    def test_no_login(self, faker, api_client):
        entry = EntryFactory.create()
        response = api_client.post(
            reverse('api:entry-comment-add', kwargs={'pk': entry.id}),
            {'comment': faker.text()}
        )
        assert response.status_code == 401
        assert MyComment.objects.count() == 0

    @pytest.mark.django_db
    def test_no_required_field(self, faker, user_client):
        entry = EntryFactory.create()
        response = user_client.post(
            reverse('api:entry-comment-add', kwargs={'pk': entry.id}),
            {'blah': faker.text()}
        )
        assert response.status_code == 400
        assert MyComment.objects.count() == 0

    @pytest.mark.django_db
    def test_success(self, faker, user_client, existed_user):
        entry = EntryFactory.create()
        comment_text = faker.text()
        response = user_client.post(
            reverse('api:entry-comment-add', kwargs={'pk': entry.id}),
            {'text': comment_text}
        )
        assert response.status_code == 202
        comment = MyComment.objects.first()
        assert comment.user == existed_user
        assert comment.text == comment_text


class TestEntryCommentRemove:

    @pytest.mark.django_db
    def test_no_login(self, faker, api_client):
        my_comment = MyCommentFactory.create()
        response = api_client.post(
            reverse('api:entry-comment-remove',
                    kwargs={'pk': my_comment.entry_id}),
            {'comment_id': faker.pyint()}
        )
        assert response.status_code == 401
        assert MyComment.objects.count() == 1

    @pytest.mark.django_db
    def test_no_required_field(self, user_client, existed_user):
        my_comment = MyCommentFactory.create(user=existed_user)
        response = user_client.post(
            reverse('api:entry-comment-remove',
                    kwargs={'pk': my_comment.entry_id}),
            {'blah': my_comment.id}
        )
        assert response.status_code == 400
        assert MyComment.objects.count() == 1

    @pytest.mark.django_db
    def test_success_remove(self, user_client, existed_user):
        my_comment = MyCommentFactory.create(user=existed_user)
        MyCommentFactory.create()
        assert MyComment.objects.count() == 2
        response = user_client.post(
            reverse('api:entry-comment-remove',
                    kwargs={'pk': my_comment.entry_id}),
            {'comment_id': my_comment.id}
        )
        assert response.status_code == 202
        assert MyComment.objects.count() == 1


class TestMyEntriesList:

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        response = api_client.get(reverse('api:my-entry-list'))
        assert response.status_code == 401

    @pytest.mark.django_db
    def test_list(self, user_client, existed_user):
        my_feed = MyFeedFactory.create(user=existed_user)
        EntryFactory.create_batch(5, feed_id=my_feed.feed_id)
        EntryFactory.create_batch(5)
        response = user_client.get(reverse('api:my-entry-list'))
        assert response.status_code == 200
        assert response.data['count'] == 5

    @pytest.mark.django_db
    def test_list_with_query(self, user_client, existed_user):
        my_feed = MyFeedFactory.create(user=existed_user)
        my_feed2 = MyFeedFactory.create(user=existed_user)
        EntryFactory.create_batch(5, feed_id=my_feed.feed_id)
        EntryFactory.create_batch(5, feed_id=my_feed2.feed_id)
        EntryFactory.create_batch(5)
        response = user_client.get(
            f'{reverse("api:my-entry-list")}?feed_id={my_feed.feed_id}'
        )
        assert response.status_code == 200
        assert response.data['count'] == 5

    @pytest.mark.django_db
    def test_no_entries(self, user_client, existed_user):
        MyFeedFactory.create(user=existed_user)
        EntryFactory.create_batch(5)
        response = user_client.get(reverse('api:my-entry-list'))
        assert response.status_code == 200
        assert response.data['count'] == 0


class TestMySavedEntriesList:

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        MySavedEntryFactory.create_batch(5)
        response = api_client.get(reverse('api:my-saved-entry'))
        assert response.status_code == 401

    @pytest.mark.django_db
    def test_no_entries(self, user_client):
        MySavedEntryFactory.create_batch(5)
        response = user_client.get(reverse('api:my-saved-entry'))
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        MySavedEntryFactory.create_batch(5)
        MySavedEntryFactory.create_batch(5, user=existed_user)
        response = user_client.get(reverse('api:my-saved-entry'))
        assert response.status_code == 200
        assert len(response.data) == 5


class TestMyStarredEntriesList:

    @pytest.mark.django_db
    def test_entries_list_stars_field(self, api_client):
        entry = EntryFactory.create()
        MyStarredEntryFactory.create_batch(5, entry=entry)
        response = api_client.get(reverse('api:entry-list'))
        assert response.status_code == 200
        assert response.data[0]['stars'] == 5

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        MyStarredEntryFactory.create_batch(5)
        response = api_client.get(reverse('api:my-starred-entry'))
        assert response.status_code == 401

    @pytest.mark.django_db
    def test_no_entries(self, user_client):
        MyStarredEntryFactory.create_batch(5)
        response = user_client.get(reverse('api:my-starred-entry'))
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        MySavedEntryFactory.create_batch(5)
        MySavedEntryFactory.create_batch(5, user=existed_user)
        response = user_client.get(reverse('api:my-saved-entry'))
        assert response.status_code == 200
        assert len(response.data) == 5


class TestMyEntrySave:

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        entry = EntryFactory.create()
        response = api_client.post(
            reverse('api:my-entry-save'), {'entry_id': entry.id}
        )
        assert response.status_code == 401
        assert MySavedEntry.objects.count() == 0

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        entry = EntryFactory.create()
        assert MySavedEntry.objects.count() == 0
        response = user_client.post(
            reverse('api:my-entry-save'),
            {'entry_id': entry.id}
        )
        assert response.status_code == 202
        my_saved_entry = MySavedEntry.objects.first()
        assert my_saved_entry.user == existed_user
        assert my_saved_entry.entry == entry


class TestMyEntryRemove:

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        my_saved_entry = MySavedEntryFactory()
        response = api_client.post(
            reverse('api:my-entry-remove'),
            {'entry_id': my_saved_entry.entry_id}
        )
        assert response.status_code == 401
        assert MySavedEntry.objects.count() == 1

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        MySavedEntryFactory.create()
        my_saved_entry = MySavedEntryFactory.create(user=existed_user)
        assert MySavedEntry.objects.count() == 2
        response = user_client.post(
            reverse('api:my-entry-remove'),
            {'entry_id': my_saved_entry.entry_id}
        )
        assert response.status_code == 202
        assert MySavedEntry.objects.count() == 1


class TestMyEntryStar:

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        entry = EntryFactory.create()
        response = api_client.post(
            reverse('api:my-entry-star'), {'entry_id': entry.id}
        )
        assert response.status_code == 401
        assert MyStarredEntry.objects.count() == 0

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        entry = EntryFactory.create()
        assert MyStarredEntry.objects.count() == 0
        response = user_client.post(
            reverse('api:my-entry-star'),
            {'entry_id': entry.id}
        )
        assert response.status_code == 202
        my_starred_entry = MyStarredEntry.objects.first()
        assert my_starred_entry.user == existed_user
        assert my_starred_entry.entry == entry


class TestMyEntryUnstar:

    @pytest.mark.django_db
    def test_no_login(self, api_client):
        my_starred_entry = MyStarredEntryFactory()
        response = api_client.post(
            reverse('api:my-entry-unstar'),
            {'entry_id': my_starred_entry.entry_id}
        )
        assert response.status_code == 401
        assert MyStarredEntry.objects.count() == 1

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        MyStarredEntryFactory.create()
        my_starred_entry = MyStarredEntryFactory.create(user=existed_user)
        assert MyStarredEntry.objects.count() == 2
        response = user_client.post(
            reverse('api:my-entry-unstar'),
            {'entry_id': my_starred_entry.entry_id}
        )
        assert response.status_code == 202
        assert MyStarredEntry.objects.count() == 1
