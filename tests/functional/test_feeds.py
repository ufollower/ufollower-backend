import pytest
from django.urls.base import reverse

from ufollower.apps.feeds.models import Feed
from ufollower.apps.feeds.parse import FeedError
from ufollower.apps.feeds.parse import ParsedFeed
from ufollower.apps.feeds.parse import ParseError
from ufollower.apps.subscriptions.models import MyFeed

from ..factories import FeedFactory
from ..factories import MyFeedFactory


class TestFeedsList:

    @pytest.mark.django_db
    def test_success_no_login(self, api_client):
        FeedFactory.create_batch(5)
        response = api_client.get(reverse('api:feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_no_feeds_no_login(self, api_client):
        response = api_client.get(reverse('api:feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_success_with_login(self, user_client):
        FeedFactory.create_batch(5)
        response = user_client.get(reverse('api:feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_success_no_feeds_with_login(self, user_client):
        response = user_client.get(reverse('api:feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 0


class TestMyFeedList:

    @pytest.mark.django_db
    def test_not_authorized(self, api_client):
        response = api_client.get(reverse('api:my-feed-list'))
        assert response.status_code == 401

    @pytest.mark.django_db
    def test_no_my_feeds(self, user_client):
        FeedFactory.create_batch(5)
        response = user_client.get(reverse('api:my-feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 0

    @pytest.mark.django_db
    def test_success_my_feeds(self, user_client, existed_user):
        FeedFactory.create_batch(5)
        MyFeedFactory.create_batch(5, user=existed_user)
        response = user_client.get(reverse('api:my-feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 5

    @pytest.mark.django_db
    def test_is_followed_field(self, user_client, existed_user):
        FeedFactory.create_batch(5)
        MyFeedFactory.create_batch(5, user=existed_user)
        response = user_client.get(reverse('api:feed-list'))
        assert response.status_code == 200
        assert len(response.data) == 10
        followed_feeds = list(
            filter(lambda feed: feed['is_followed'], response.data)
        )
        assert len(followed_feeds) == 5


class TestMyFeedFollow:

    @pytest.mark.django_db
    def test_not_authorized(self, api_client):
        response = api_client.post(reverse('api:my-feed-follow'), {})
        assert response.status_code == 401

    @pytest.mark.django_db
    def test_wrong_methods(self, user_client):
        response = user_client.get(reverse('api:my-feed-follow'))
        assert response.status_code == 405

        response = user_client.put(
            reverse('api:my-feed-follow'), {'feed_id': 1}
        )
        assert response.status_code == 405

    @pytest.mark.django_db
    def test_no_required_fields(self, user_client):
        response = user_client.post(
            reverse('api:my-feed-follow'), {'wrong_field': 1}
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    def test_both_id_and_url_fields_in_request(self, faker, user_client):
        response = user_client.post(
            reverse('api:my-feed-follow'),
            {'feed_id': 1, 'feed_url': faker.url()}
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    def test_feed_id_not_existed(self, user_client):
        feed = FeedFactory()
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_id': feed.id + 1}
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    def test_feed_id_existed(self, user_client, existed_user):
        feed = FeedFactory()
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_id': feed.id}
        )
        assert response.status_code == 202
        my_feed = MyFeed.objects.get(user=existed_user)
        assert my_feed.feed_id == feed.id

    @pytest.mark.django_db
    def test_feed_url_existed(self, mocker, user_client, existed_user):
        feed = FeedFactory()
        mock_parser = mocker.patch(
            'ufollower.apps.api.feeds.views.parse_feed'
        )
        mock_parser.return_value = ParsedFeed(
            feed_url=feed.feed_url,
            title=feed.title,
            description=feed.description,
            link=feed.link,
        )
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_url': feed.feed_url}
        )
        assert response.status_code == 202
        my_feed = MyFeed.objects.get(user=existed_user)
        assert my_feed.feed_id == feed.id

    @pytest.mark.django_db
    def test_feed_url_parsed_error(self, mocker, user_client, existed_user):
        feed = FeedFactory()
        mock_parser = mocker.patch(
            'ufollower.apps.api.feeds.views.parse_feed'
        )
        mock_parser.side_effect = ParseError()
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_url': feed.feed_url}
        )
        assert response.status_code == 400
        mock_parser.side_effect = FeedError()
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_url': feed.feed_url}
        )
        assert response.status_code == 400
        assert MyFeed.objects.count() == 0

    @pytest.mark.django_db
    def test_feed_url_new_feed(self, mocker, faker, user_client, existed_user):
        parsed_feed = ParsedFeed(
            feed_url=faker.url(),
            title=faker.sentence(),
            description=faker.text(),
            link=faker.uri(),
        )
        mock_parser = mocker.patch(
            'ufollower.apps.api.feeds.views.parse_feed'
        )
        mock_parser.return_value = parsed_feed
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_url': parsed_feed.feed_url}
        )
        assert response.status_code == 202
        feed = Feed.objects.get(feed_url=parsed_feed.feed_url)
        assert feed.title == parsed_feed.title
        assert feed.link == parsed_feed.link
        assert feed.description == parsed_feed.description
        MyFeed.objects.get(feed=feed, user=existed_user)

    @pytest.mark.django_db
    def test_already_followed(self, mocker, user_client, existed_user):
        feed = FeedFactory.create()
        mock_parser = mocker.patch(
            'ufollower.apps.api.feeds.views.parse_feed'
        )
        mock_parser.return_value = ParsedFeed(
            feed_url=feed.feed_url,
            title=feed.title,
            description=feed.description,
            link=feed.link,
        )
        MyFeedFactory.create(user=existed_user, feed=feed)
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_url': feed.feed_url}
        )
        assert response.status_code == 304
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_id': feed.id}
        )
        assert response.status_code == 304

    @pytest.mark.django_db
    def test_is_followed_field(self, user_client, existed_user):
        feed = FeedFactory()
        response = user_client.get(reverse('api:feed-list'))
        assert response.data[0]['is_followed'] is False
        response = user_client.post(
            reverse('api:my-feed-follow'), {'feed_id': feed.id}
        )
        assert response.status_code == 202
        my_feed = MyFeed.objects.get(user=existed_user)
        assert my_feed.feed_id == feed.id
        response = user_client.get(reverse('api:feed-list'))
        assert response.data[0]['is_followed'] is True


class TestMyFeedUnfollow:

    @pytest.mark.django_db
    def test_not_authorized(self, api_client):
        response = api_client.get(reverse('api:my-feed-unfollow'))
        assert response.status_code == 401

    @pytest.mark.django_db
    def test_no_required_fields(self, user_client):
        response = user_client.post(
            reverse('api:my-feed-unfollow'), {'wrong_field': 1}
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    def test_not_existed_feed(self, user_client):
        response = user_client.post(
            reverse('api:my-feed-unfollow'), {'feed_id': 1}
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    def test_not_followed_feed(self, user_client):
        feed = FeedFactory()
        response = user_client.post(
            reverse('api:my-feed-unfollow'), {'feed_id': feed.id}
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    def test_success(self, user_client, existed_user):
        feed = FeedFactory.create()
        MyFeedFactory.create(user=existed_user, feed=feed)
        assert MyFeed.objects.filter(user=existed_user).count() == 1
        response = user_client.post(
            reverse('api:my-feed-unfollow'), {'feed_id': feed.id}
        )
        assert response.status_code == 202
        assert MyFeed.objects.filter(user=existed_user).count() == 0

    @pytest.mark.django_db
    def test_is_followed_field(self, user_client, existed_user):
        feed = FeedFactory.create()
        MyFeedFactory.create(user=existed_user, feed=feed)
        response = user_client.get(reverse('api:feed-list'))
        assert response.data[0]['is_followed'] is True
        user_client.post(
            reverse('api:my-feed-unfollow'), {'feed_id': feed.id}
        )
        response = user_client.get(reverse('api:feed-list'))
        assert response.data[0]['is_followed'] is False
