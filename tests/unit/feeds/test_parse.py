import pytest
from feedparser import FeedParserDict

from ufollower.apps.feeds.parse import FeedError
from ufollower.apps.feeds.parse import ParseError
from ufollower.apps.feeds.parse import parse_feed


class TestParseFeed:
    def test_parse_error(self, faker, mocker):
        feedparser_result = FeedParserDict()
        feedparser_result.status = faker.pyint(min_value=400, max_value=599)
        mock_feedparser = mocker.patch(
            'ufollower.apps.feeds.parse.feedparser.parse'
        )
        mock_feedparser.return_value = feedparser_result
        with pytest.raises(ParseError):
            parse_feed(faker.uri())

    def test_feed_error(self, faker, mocker):
        feedparser_result = FeedParserDict()
        feedparser_result.status = 200
        feedparser_result.feed = FeedParserDict()
        mock_feedparser = mocker.patch(
            'ufollower.apps.feeds.parse.feedparser.parse'
        )
        mock_feedparser.return_value = feedparser_result
        with pytest.raises(FeedError):
            parse_feed(faker.uri())

    def test_parsed_feed_from_parse_feed_func(self, faker, mocker):
        feed_url = faker.url()
        feed_title = faker.sentence()
        feed_link = faker.url()
        feed_description = faker.text()
        feedparser_result = FeedParserDict()
        feedparser_result.status = 200
        feedparser_result.feed = FeedParserDict()
        feedparser_result.feed['title'] = feed_title
        feedparser_result.feed['link'] = feed_link
        feedparser_result.feed['description'] = feed_description
        feedparser_result.feed['links'] = [
            FeedParserDict(rel='self', href=feed_url),
            FeedParserDict(rel='alternate', href=faker.url())
        ]
        feedparser_result.entries = []

        mock_feedparser = mocker.patch(
            'ufollower.apps.feeds.parse.feedparser.parse'
        )
        mock_feedparser.return_value = feedparser_result
        parsed_feed = parse_feed(faker.uri())
        assert parsed_feed.feed_url == feed_url
        assert parsed_feed.title == feed_title
        assert parsed_feed.link == feed_link
        assert parsed_feed.description == feed_description
