import pytest

from ufollower.apps.feeds.exceptions import FeedError
from ufollower.apps.feeds.exceptions import ParseError
from ufollower.apps.feeds.models import Entry
from ufollower.apps.feeds.models import Feed

from ...factories import ParsedEntryFactory
from ...factories import ParsedFeedFactory


@pytest.fixture()
def feed(db, faker):
    return Feed.objects.create(feed_url=faker.uri())


class TestFeedModel:

    @pytest.mark.django_db
    def test_str(self, feed):
        assert str(feed) == feed.feed_url

    @pytest.mark.django_db
    def test_update_failed_parse(self, mocker, feed):
        mocked_parse = mocker.patch('ufollower.apps.feeds.models.parse_feed')
        mocked_parse.side_effect = ParseError()
        with pytest.raises(ParseError):
            feed.update()
        new_feed = Feed.objects.get(feed_url=feed.feed_url)
        assert feed == new_feed
        mocked_parse.side_effect = FeedError()
        with pytest.raises(FeedError):
            feed.update()
        new_feed = Feed.objects.get(feed_url=feed.feed_url)
        assert feed == new_feed

    @pytest.mark.django_db
    def test_update_feed_info(self, mocker, feed):
        mocked_parse = mocker.patch('ufollower.apps.feeds.models.parse_feed')
        parsed_feed = ParsedFeedFactory.create()
        mocked_parse.return_value = parsed_feed
        feed.update()
        feed.refresh_from_db()
        assert feed.title == parsed_feed.title
        assert feed.link == parsed_feed.link
        assert feed.description == parsed_feed.description
        assert feed.last_fetched_at is not None

    @pytest.mark.django_db
    def test_update_feed_with_new_entries(self, mocker, feed):
        mocked_parse = mocker.patch('ufollower.apps.feeds.models.parse_feed')
        parsed_feed = ParsedFeedFactory.create()
        parsed_feed.entries = ParsedEntryFactory.create_batch(5)
        mocked_parse.return_value = parsed_feed
        feed.update()
        assert Entry.objects.count() == 5
        assert Entry.objects.filter(feed=feed).count() == 5

    @pytest.mark.django_db
    def test_update_feed_with_existed_entry(self, faker, mocker, feed):
        entry_title = faker.sentence()
        entry_link = faker.uri()
        entry_guid = faker.slug()
        Entry.objects.create(
            feed=feed, guid=entry_guid, link=entry_link, title=entry_title,
        )
        assert Entry.objects.filter(feed=feed).count() == 1
        mocked_parse = mocker.patch('ufollower.apps.feeds.models.parse_feed')
        parsed_feed = ParsedFeedFactory.create()
        # Entry with same guid
        parsed_feed.entries.append(
            ParsedEntryFactory.create(guid=entry_guid)
        )
        mocked_parse.return_value = parsed_feed
        feed.update()
        assert Entry.objects.filter(feed=feed).count() == 1
        parsed_feed.entries.pop()
        # Entry with no guid and same link
        parsed_feed.entries.append(
            ParsedEntryFactory.create(guid='', link=entry_link)
        )
        feed.update()
        assert Entry.objects.filter(feed=feed).count() == 1
        parsed_feed.entries.pop()
        # Entry with no guid and link and same title
        parsed_feed.entries.append(
            ParsedEntryFactory.create(guid='', link='', title=entry_title)
        )
        feed.update()
        assert Entry.objects.filter(feed=feed).count() == 1

    @pytest.mark.django_db
    def test_update_feed_same_entry_different_feed(self, faker, mocker, feed):
        entry_guid = faker.slug()
        Entry.objects.create(feed=feed, guid=entry_guid)
        new_feed = Feed.objects.create(feed_url=faker.url())
        assert Entry.objects.count() == 1
        mocked_parse = mocker.patch('ufollower.apps.feeds.models.parse_feed')
        parsed_feed = ParsedFeedFactory.create()
        # Entry with same guid
        parsed_feed.entries.append(
            ParsedEntryFactory.create(guid=entry_guid)
        )
        mocked_parse.return_value = parsed_feed
        new_feed.update()
        assert Entry.objects.count() == 2
