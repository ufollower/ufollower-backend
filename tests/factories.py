import factory
from django.contrib.auth import get_user_model
from factory.fuzzy import FuzzyText

from ufollower.apps.feeds.models import Entry
from ufollower.apps.feeds.models import Feed
from ufollower.apps.feeds.parse import ParsedEntry
from ufollower.apps.feeds.parse import ParsedFeed
from ufollower.apps.subscriptions.models import MyComment
from ufollower.apps.subscriptions.models import MyFeed
from ufollower.apps.subscriptions.models import MyNewEntry
from ufollower.apps.subscriptions.models import MySavedEntry
from ufollower.apps.subscriptions.models import MyStarredEntry

User = get_user_model()


class UserFactory(factory.DjangoModelFactory):
    username = factory.Faker('user_name')

    class Meta:
        model = User


class FeedFactory(factory.DjangoModelFactory):
    feed_url = factory.LazyAttribute(lambda obj: f'{obj.link}{obj.title}.rss')
    title = factory.Sequence(lambda n: f'feed-{n}')
    description = FuzzyText(length=100)
    link = factory.Faker('url')
    is_active = True

    class Meta:
        model = Feed


class EntryFactory(factory.DjangoModelFactory):
    feed = factory.SubFactory(FeedFactory)
    guid = factory.LazyAttribute(lambda obj: obj.title)
    title = factory.Sequence(lambda n: f'entry-{n}')
    description = FuzzyText(length=100)
    link = factory.Faker('uri')

    class Meta:
        model = Entry


class MyFeedFactory(factory.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    feed = factory.SubFactory(FeedFactory)

    class Meta:
        model = MyFeed


class AbstractEntryFactory(factory.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    entry = factory.SubFactory(EntryFactory)

    class Meta:
        abstract = True


class MyNewEntryFactory(AbstractEntryFactory):

    class Meta:
        model = MyNewEntry


class MySavedEntryFactory(AbstractEntryFactory):

    class Meta:
        model = MySavedEntry


class MyStarredEntryFactory(AbstractEntryFactory):

    class Meta:
        model = MyStarredEntry


class MyCommentFactory(AbstractEntryFactory):
    text = FuzzyText(length=50)

    class Meta:
        model = MyComment


class ParsedFeedFactory(factory.Factory):
    class Meta:
        model = ParsedFeed

    feed_url = factory.Faker('url')
    title = factory.Faker('sentence')
    link = factory.Faker('uri')
    description = factory.Faker('text')


class ParsedEntryFactory(factory.Factory):
    class Meta:
        model = ParsedEntry

    guid = factory.Sequence(lambda n: f'entry-{n}')
    title = factory.Faker('sentence')
    link = factory.Faker('uri')
    description = factory.Faker('text')
