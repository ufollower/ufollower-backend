# ufollower-backend

[![pipeline status](https://gitlab.com/ufollower/ufollower-backend/badges/master/pipeline.svg)](https://gitlab.com/ufollower/ufollower-backend/commits/master)
[![coverage report](https://gitlab.com/ufollower/ufollower-backend/badges/master/coverage.svg)](https://gitlab.com/ufollower/ufollower-backend/commits/master)


Also read:

[Development doc](./DEVELOPMENT.md)

Additional links:

[Ufollower Frontend](https://gitlab.com/urollower/ufollower-frontend/)