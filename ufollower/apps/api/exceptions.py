from rest_framework.exceptions import APIException
from rest_framework.status import HTTP_409_CONFLICT


class UserExists(APIException):
    status_code = HTTP_409_CONFLICT
    default_detail = 'User already exists'
    default_code = 'user_exists'
