from django.urls import path

from . import views

urlpatterns = [
    path('', views.feed_list_view, name='feed-list'),
    path('my/', views.my_feed_list_view, name='my-feed-list'),
    path('follow/', views.feed_follow_view, name='my-feed-follow'),
    path('unfollow/', views.feed_unfollow_view, name='my-feed-unfollow'),
]
