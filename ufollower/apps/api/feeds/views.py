from django.db.models.aggregates import Count
from django.db.models.expressions import Exists
from django.db.models.expressions import OuterRef
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ufollower.apps.feeds.models import Feed
from ufollower.apps.feeds.parse import FeedError
from ufollower.apps.feeds.parse import ParseError
from ufollower.apps.feeds.parse import parse_feed
from ufollower.apps.subscriptions.models import MyFeed

from .serializers import FeedSerializer
from .serializers import MyFeedFollowSerializer
from .serializers import MyFeedUnfollowSerializer


class FeedListView(ListAPIView):
    serializer_class = FeedSerializer

    def get_queryset(self):
        queryset = Feed.objects.filter(is_active=True).annotate(
            entries_count=Count('entries')
        )
        if self.request.user.is_authenticated:
            my_feeds = MyFeed.objects.filter(
                user=self.request.user,
                feed=OuterRef('pk'),
            )
            queryset = queryset.annotate(is_followed=Exists(my_feeds))
        return queryset


class MyFeedListView(FeedListView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(myfeed__user=self.request.user)
        return queryset


class FeedFollowView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = MyFeedFollowSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        feed_url = serializer.validated_data.get('feed_url')
        feed_id = serializer.validated_data.get('feed_id')

        if feed_url:
            try:
                parsed_feed = parse_feed(feed_url)
            except (ParseError, FeedError):
                return Response(
                    {'detail': 'Unable to parse feed'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            try:
                feed = Feed.objects.get(feed_url=parsed_feed.feed_url)
            except Feed.DoesNotExist:
                feed = Feed.objects.create(
                    feed_url=parsed_feed.feed_url,
                    title=parsed_feed.title,
                    link=parsed_feed.link,
                    description=parsed_feed.description,
                    is_active=True,
                )

            feed_id = feed.id

        _, created = MyFeed.objects.get_or_create(
            user=request.user,
            feed_id=feed_id,
        )
        if not created:
            return Response({}, status=status.HTTP_304_NOT_MODIFIED)
        return Response({}, status=status.HTTP_202_ACCEPTED)


class FeedUnfollowView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = MyFeedUnfollowSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        feed_id = serializer.validated_data.get('feed_id')

        try:
            feed = MyFeed.objects.get(user=request.user, feed_id=feed_id)
            feed.delete()
            return Response({}, status=status.HTTP_202_ACCEPTED)
        except MyFeed.DoesNotExist:
            return Response(
                {'detail': 'User is not following feed'},
                status=status.HTTP_400_BAD_REQUEST
            )


feed_list_view = FeedListView.as_view()
my_feed_list_view = MyFeedListView.as_view()
feed_follow_view = FeedFollowView.as_view()
feed_unfollow_view = FeedUnfollowView.as_view()
