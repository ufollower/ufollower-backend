from rest_framework import serializers

from ufollower.apps.feeds.models import Feed


class FeedSerializer(serializers.ModelSerializer):
    entries_count = serializers.IntegerField(read_only=True)
    is_followed = serializers.BooleanField(default=False)

    class Meta:
        model = Feed
        fields = [
            'id', 'title', 'description', 'link', 'feed_url', 'is_active',
            'entries_count', 'is_followed',
        ]


class MyFeedFollowSerializer(serializers.Serializer):
    feed_id = serializers.IntegerField(required=False)
    feed_url = serializers.URLField(required=False)

    def validate_feed_id(self, value):
        if not Feed.objects.filter(id=value).exists():
            raise serializers.ValidationError('No feed found with this id')
        return value

    def validate(self, attrs):
        if all((attrs.get('feed_id'), attrs.get('feed_url'))):
            raise serializers.ValidationError('Only one field must be provided')

        if not any((attrs.get('feed_id'), attrs.get('feed_url'))):
            raise serializers.ValidationError(
                'Either feed_id or feed_url must be provided'
            )
        return attrs


class MyFeedUnfollowSerializer(serializers.Serializer):
    feed_id = serializers.IntegerField()

    def validate_feed_id(self, value):
        if not Feed.objects.filter(id=value).exists():
            raise serializers.ValidationError('No feed found with this id')
        return value
