from django.urls import path

from . import views

urlpatterns = [
    path('profile/', views.profile_view, name='profile'),
    path('sign-in/', views.signin_view, name='sign-in'),
    path('sign-out/', views.signout_view, name='sign-out'),
    path('sign-up/', views.signup_view, name='sign-up'),
    path(
        'password-change/',
        views.password_change_view,
        name='password-change'
    ),
]
