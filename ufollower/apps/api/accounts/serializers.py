from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.exceptions import NotAuthenticated

from ..exceptions import UserExists

User = get_user_model()


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class SignInSerializer(serializers.Serializer):
    username = serializers.CharField(label='Username', required=True)
    password = serializers.CharField(label='Password', required=True)

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        request = self.context.get('request')
        if username and password:
            user = authenticate(
                request=request,
                username=username,
                password=password
            )
            if not user:
                raise AuthenticationFailed()
        else:
            raise NotAuthenticated()

        attrs['user'] = user
        return attrs


class SignUpSerializer(serializers.Serializer):
    username = serializers.CharField(label='Username', required=True)
    password = serializers.CharField(label='Password', required=True)

    def validate(self, attrs):
        user_exists = User.objects.filter(username=attrs['username']).exists()
        if user_exists:
            raise UserExists()
        return attrs

    def create(self, validated_data):
        user = User.objects.create(username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user


class PasswordChangeSerializer(serializers.Serializer):
    old_password = serializers.CharField(label='Old password', required=True)
    new_password = serializers.CharField(label='New password', required=True)
