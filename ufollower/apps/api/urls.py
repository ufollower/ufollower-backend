from django.urls import include
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from .accounts import urls as account_urls
from .entries import urls as entries_urls
from .feeds import urls as feeds_urls

app_name = 'api'
schema_view = get_schema_view(
    openapi.Info(
        title='UFollower API',
        default_version='v1',
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('accounts/', include(account_urls)),
    path('feeds/', include(feeds_urls)),
    path('entries/', include(entries_urls)),
    path('', schema_view.with_ui('swagger', cache_timeout=0)),
]
