from django.apps import AppConfig


class ApiAppConfig(AppConfig):
    name = 'ufollower.apps.api'
    verbose_name = 'API'
