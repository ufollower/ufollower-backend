from django.urls import path

from . import views

urlpatterns = [
    path('', views.entry_list_view, name='entry-list'),
    path('<int:pk>/', views.entry_detail_view, name='entry-detail'),
    path(
        '<int:pk>/comments/',
        views.entry_comment_list_view,
        name='entry-comment-list'
    ),
    path(
        '<int:pk>/comments/add/',
        views.entry_comment_add_view,
        name='entry-comment-add'
    ),
    path(
        '<int:pk>/comments/remove/',
        views.entry_comment_remove_view,
        name='entry-comment-remove'
    ),
    path('my/', views.my_entry_list_view, name='my-entry-list'),
    path('saved/', views.entry_saved_list_view, name='my-saved-entry'),
    path('starred/', views.entry_starred_list_view, name='my-starred-entry'),
    path('save/', views.entry_save_view, name='my-entry-save'),
    path('remove/', views.entry_remove_view, name='my-entry-remove'),
    path('star/', views.entry_star_view, name='my-entry-star'),
    path('unstar/', views.entry_unstar_view, name='my-entry-unstar'),
]
