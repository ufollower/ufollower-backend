from collections import OrderedDict

from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response


class CustomLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 50

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.count),
            ('next_offset', self.get_next_offset()),
            ('results', data),
        ]))

    def get_paginated_response_schema(self, schema):
        return {
            'type': 'object',
            'properties': {
                'count': {
                    'type': 'integer',
                    'example': 123,
                },
                'next_offset': {
                    'type': 'integer',
                    'example': 3,
                    'nullable': True,
                },
                'results': schema,
            },
        }

    def get_next_offset(self):
        if self.offset + self.limit >= self.count:
            return None

        offset = self.offset + self.limit
        return offset
