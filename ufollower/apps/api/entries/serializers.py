from rest_framework import serializers

from ufollower.apps.feeds.models import Entry
from ufollower.apps.subscriptions.models import MyComment


class EntryShortSerializer(serializers.ModelSerializer):
    comments = serializers.IntegerField()
    stars = serializers.IntegerField()
    is_new = serializers.BooleanField(default=False)

    class Meta:
        model = Entry
        fields = ['id', 'title', 'pub_date', 'comments', 'stars', 'is_new']


class EntryFullSerializer(serializers.ModelSerializer):
    comments = serializers.IntegerField()
    stars = serializers.IntegerField()

    class Meta:
        model = Entry
        fields = [
            'id', 'title', 'description', 'link', 'pub_date',
            'comments', 'stars',
        ]


class EntryCommentSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')

    class Meta:
        model = MyComment
        fields = ['id', 'entry_id', 'text', 'created_at', 'username']


class EntryCommentAddSerializer(serializers.Serializer):
    text = serializers.CharField(required=True)


class EntryCommentRemoveSerializer(serializers.Serializer):
    comment_id = serializers.IntegerField(required=True)


class MyEntrySaveSerializer(serializers.Serializer):
    entry_id = serializers.IntegerField()

    def validate_entry_id(self, value):
        if not Entry.objects.filter(id=value).exists():
            raise serializers.ValidationError('No entry found with this id')
        return value


class MyEntryStarSerializer(serializers.Serializer):
    entry_id = serializers.IntegerField()

    def validate_entry_id(self, value):
        if not Entry.objects.filter(id=value).exists():
            raise serializers.ValidationError('No entry found with this id')
        return value
