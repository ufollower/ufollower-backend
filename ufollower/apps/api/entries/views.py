from django.db.models.aggregates import Count
from django.db.models.expressions import Exists
from django.db.models.expressions import OuterRef
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ufollower.apps.feeds.models import Entry
from ufollower.apps.subscriptions.models import MyComment
from ufollower.apps.subscriptions.models import MyNewEntry
from ufollower.apps.subscriptions.models import MySavedEntry
from ufollower.apps.subscriptions.models import MyStarredEntry

from . import serializers
from .pagination import CustomLimitOffsetPagination


class EntryListView(ListAPIView):
    serializer_class = serializers.EntryShortSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        queryset = Entry.objects.annotate(
            comments=Count('mycomment'),
            stars=Count('mystarredentry'),
        ).order_by('-pub_date', 'title')

        feed_id = self.request.query_params.get('feed_id')
        if feed_id:
            queryset = queryset.filter(
                feed_id=feed_id
            )
        if self.request.user.is_authenticated:
            new_entries = MyNewEntry.objects.filter(
                user=self.request.user,
                entry=OuterRef('pk'),
            )
            queryset = queryset.annotate(is_new=Exists(new_entries))
        return queryset


class EntryDetailView(RetrieveAPIView):
    serializer_class = serializers.EntryFullSerializer

    def get_object(self):
        obj = super().get_object()
        if self.request.user.is_authenticated:
            try:
                MyNewEntry.objects.get(
                    user=self.request.user,
                    entry=obj,
                ).delete()
            except MyNewEntry.DoesNotExist:
                pass
        return obj

    def get_queryset(self):
        queryset = Entry.objects.annotate(
            comments=Count('mycomment'),
            stars=Count('mystarredentry'),
        )
        return queryset


class MyEntryList(EntryListView):
    permission_classes = [IsAuthenticated]
    pagination_class = CustomLimitOffsetPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(feed__myfeed__user=self.request.user)
        return queryset


class EntrySavedListView(EntryListView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(mysavedentry__user=self.request.user)
        return queryset


class EntryStarredListView(EntryListView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(mystarredentry__user=self.request.user)
        return queryset


class EntrySaveView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.MyEntrySaveSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        entry_id = serializer.validated_data.get('entry_id')

        _, created = MySavedEntry.objects.get_or_create(
            user=request.user,
            entry_id=entry_id,
        )
        if not created:
            return Response({}, status=status.HTTP_304_NOT_MODIFIED)
        return Response({}, status=status.HTTP_202_ACCEPTED)


class EntryRemoveView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.MyEntrySaveSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        entry_id = serializer.validated_data.get('entry_id')

        try:
            saved_entry = MySavedEntry.objects.get(
                user=request.user, entry_id=entry_id,
            )
            saved_entry.delete()
            return Response({}, status=status.HTTP_202_ACCEPTED)
        except MySavedEntry.DoesNotExist:
            return Response(
                {'detail': 'User did not save such entry'},
                status=status.HTTP_400_BAD_REQUEST
            )


class EntryStarView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.MyEntryStarSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        entry_id = serializer.validated_data.get('entry_id')

        _, created = MyStarredEntry.objects.get_or_create(
            user=request.user,
            entry_id=entry_id,
        )
        if not created:
            return Response({}, status=status.HTTP_304_NOT_MODIFIED)
        return Response({}, status=status.HTTP_202_ACCEPTED)


class EntryUnstarView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.MyEntryStarSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        entry_id = serializer.validated_data.get('entry_id')

        try:
            saved_entry = MyStarredEntry.objects.get(
                user=request.user, entry_id=entry_id,
            )
            saved_entry.delete()
            return Response({}, status=status.HTTP_202_ACCEPTED)
        except MyStarredEntry.DoesNotExist:
            return Response(
                {'detail': 'User did not star such entry'},
                status=status.HTTP_400_BAD_REQUEST
            )


class EntryCommentListView(ListAPIView):
    serializer_class = serializers.EntryCommentSerializer

    def get_queryset(self):
        queryset = MyComment.objects.filter(entry_id=self.kwargs['pk'])
        return queryset


class EntryCommentAddView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.EntryCommentAddSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        entry_id = self.kwargs['pk']
        comment_text = serializer.validated_data['text']
        MyComment.objects.create(
            user=request.user,
            entry_id=entry_id,
            text=comment_text,
        )
        return Response({}, status=status.HTTP_202_ACCEPTED)


class EntryCommentRemoveView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.EntryCommentRemoveSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        entry_id = self.kwargs['pk']
        comment_id = serializer.validated_data['comment_id']
        try:
            my_comment = MyComment.objects.get(
                id=comment_id,
                user=request.user,
                entry_id=entry_id,
            )
            my_comment.delete()
        except MyComment.DoesNotExist:
            return Response(
                {'detail': 'No comment found'},
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response({}, status=status.HTTP_202_ACCEPTED)


entry_list_view = EntryListView.as_view()
entry_detail_view = EntryDetailView.as_view()
my_entry_list_view = MyEntryList.as_view()
entry_saved_list_view = EntrySavedListView.as_view()
entry_starred_list_view = EntryStarredListView.as_view()
entry_save_view = EntrySaveView.as_view()
entry_remove_view = EntryRemoveView.as_view()
entry_star_view = EntryStarView.as_view()
entry_unstar_view = EntryUnstarView.as_view()
entry_comment_list_view = EntryCommentListView.as_view()
entry_comment_add_view = EntryCommentAddView.as_view()
entry_comment_remove_view = EntryCommentRemoveView.as_view()
