from django.apps import AppConfig


class FeedsAppConfig(AppConfig):
    name = 'ufollower.apps.feeds'
    verbose_name = 'Feeds'
