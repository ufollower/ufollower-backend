from django.contrib import admin
from django.contrib import messages

from .exceptions import FeedError
from .exceptions import ParseError
from .models import Entry
from .models import Feed
from .tasks import update_feed


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    list_display = ['feed_url', 'title', 'last_fetched_at', 'is_active']
    readonly_fields = ['title', 'last_fetched_at', 'description']
    fields = [
        'feed_url',
        'title',
        'last_fetched_at',
        'description',
        'is_active',
    ]

    actions = ['async_update_feeds', 'update_feeds']

    def async_update_feeds(self, request, queryset):
        for feed_id in queryset.values_list('id', flat=True):
            update_feed.delay(feed_id)
    async_update_feeds.short_description = 'Async update feeds'

    def update_feeds(self, request, queryset):
        for feed in queryset.iterator():
            try:
                feed.update()
            except ParseError:
                self.message_user(
                    request,
                    f'Unable to parse feed {feed.feed_url}',
                    level=messages.ERROR,
                )
            except FeedError:
                self.message_user(
                    request,
                    f'No feed found on url {feed.feed_url}',
                    messages.WARNING,
                )
    update_feeds.short_description = 'Update feeds'


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ['feed', 'title', 'link', 'pub_date']
    list_filter = ['feed']
    ordering = ('-pub_date', 'title')

    # def has_delete_permission(self, request, obj=None):
    #     return False

    def has_change_permission(self, request, obj=None):
        return False
