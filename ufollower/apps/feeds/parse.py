import datetime
import logging
import time

import feedparser

from .exceptions import EntryError
from .exceptions import FeedError
from .exceptions import ParseError

logger = logging.getLogger(__name__)


class ParsedFeed:
    def __init__(self, feed_url, title='', link='', description=''):
        self.feed_url = feed_url
        self.title = title
        self.link = link
        self.description = description
        self.entries = []

    @classmethod
    def from_feedparser(cls, feedparser_feed):
        feed_url = list(
            filter(lambda d: d['rel'] == 'self', feedparser_feed.links)
        )[0].href
        title = feedparser_feed.title
        link = feedparser_feed.link
        description = feedparser_feed.description
        return cls(feed_url, title, link, description)


class ParsedEntry:
    def __init__(
        self, guid='', title='', link='', description='', pub_date=None
    ):
        self.guid = guid
        self.title = title
        self.link = link
        self.description = description
        self.pub_date = pub_date

    @classmethod
    def from_feedparser(cls, feedparser_entry):
        guid = feedparser_entry.guid
        title = feedparser_entry.title
        link = feedparser_entry.link
        description = feedparser_entry.description
        pub_date = feedparser_entry.get('published_parsed')
        if pub_date is not None:
            pub_date = datetime.datetime.fromtimestamp(
                time.mktime(pub_date)
            )
        return cls(guid, title, link, description, pub_date)


def parse_feed(url) -> ParsedFeed:
    parsed_data = feedparser.parse(url)

    if parsed_data.status >= 400 or parsed_data.get('bozo'):
        raise ParseError()

    if not parsed_data.feed:
        raise FeedError(f'No feed found on url {url}')

    parsed_feed = ParsedFeed.from_feedparser(parsed_data.feed)

    for entry in parsed_data.entries:
        try:
            parsed_entry = ParsedEntry.from_feedparser(entry)
        except EntryError:
            continue
        parsed_feed.entries.append(parsed_entry)

    return parsed_feed
