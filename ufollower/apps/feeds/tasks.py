import datetime
import logging

from celery.exceptions import MaxRetriesExceededError
from django.conf import settings
from django.db.models import Q

from ufollower.celery import app

from .exceptions import FeedError
from .exceptions import ParseError
from .models import Feed

logger = logging.getLogger(__name__)


@app.task(name='feeds.update_feeds')
def update_feeds():
    feeds_to_update = Feed.objects.filter(
        Q(last_fetched_at__lte=datetime.datetime.now() - datetime.timedelta(
            minutes=settings.FEEDS_FETCH_INTERVAL
        )) | Q(last_fetched_at__isnull=True),
        is_active=True,
    ).values_list('id', flat=True)
    for feed_id in feeds_to_update:
        update_feed.delay(feed_id)


@app.task(bind=True, name='feeds.update_feed')
def update_feed(self, feed_id: int):
    logger.info(f'Starting update for feed: {feed_id}')
    try:
        feed = Feed.objects.get(id=feed_id)
    except Feed.DoesNotExist:
        logger.warning(f'Task received not existed feed id: {feed_id}')
        return
    logger.info(f'Updating feed {feed_id} ({feed.feed_url})')
    try:
        feed.update()
    except ParseError:
        try:
            self.retry(max_retries=3, countdown=10)
        except MaxRetriesExceededError:
            logger.error(
                f'Unable to parse feed {feed_id} ({feed.feed_url}).'
            )
            feed.disable()
    except FeedError:
        logger.warning(f'No feeds found on url {feed_id} ({feed.feed_url})')
        feed.disable()
    else:
        logger.info(f'Successfully updated feed {feed_id} ({feed.feed_url})')
