class FeedError(Exception):
    pass


class EntryError(Exception):
    pass


class ParseError(Exception):
    pass
