import logging

from django.db import models
from django.utils.timezone import now

from .parse import parse_feed

logger = logging.getLogger(__name__)


class Feed(models.Model):

    feed_url = models.URLField(max_length=2048, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    last_fetched_at = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField(default=True)

    # RSS required fields
    title = models.CharField(max_length=500, blank=True)
    link = models.URLField(max_length=2048, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.feed_url

    def disable(self):
        self.is_active = False
        logger.info(f'Feed {self.id} ({self.feed_url}) disabled.')
        self.save()

    def update(self):
        parsed_feed = parse_feed(self.feed_url)

        # Iterate in reverse order in case entry doesn't have pubDate field
        # so we could order properly according to default value
        for parsed_entry in reversed(parsed_feed.entries):
            if parsed_entry.guid:
                query = {'guid': parsed_entry.guid}
            elif parsed_entry.link:
                query = {'link': parsed_entry.link}
            else:
                query = {'title': parsed_entry.title}

            existed_entry = self.entries.filter(**query).first()
            if existed_entry:
                continue
            # TODO: add pub_date field from ParsedEntry
            entry = Entry(
                feed=self,
                guid=parsed_entry.guid,
                title=parsed_entry.title,
                link=parsed_entry.link,
                description=parsed_entry.description,
            )
            if parsed_entry.pub_date:
                entry.pub_date = parsed_entry.pub_date
            entry.save()

        self.feed_url = parsed_feed.feed_url
        self.title = parsed_feed.title
        self.description = parsed_feed.description
        self.link = parsed_feed.link
        self.last_fetched_at = now()
        self.is_active = True
        self.save()


class Entry(models.Model):

    feed = models.ForeignKey(
        Feed, on_delete=models.CASCADE, related_name='entries'
    )
    modified_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    # Entry specific fields. All of them are optional according to the rss spec.
    guid = models.CharField(max_length=500, blank=True)
    link = models.URLField(max_length=2048, blank=True)
    title = models.CharField(max_length=500, blank=True)
    description = models.TextField(blank=True)
    pub_date = models.DateTimeField(default=now)

    class Meta:
        verbose_name_plural = 'Entries'

    def __str__(self):
        return f'{self.title}'
