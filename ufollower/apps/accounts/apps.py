from django.apps import AppConfig


class AccountsAppConfig(AppConfig):
    name = 'ufollower.apps.accounts'
    verbose_name = 'Accounts'
