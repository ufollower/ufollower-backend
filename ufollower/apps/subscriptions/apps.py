from django.apps import AppConfig


class SubscriptionsAppConfig(AppConfig):
    name = 'ufollower.apps.subscriptions'
    verbose_name = 'Subscriptions'

    def ready(self):
        import ufollower.apps.subscriptions.signals  # noqa
