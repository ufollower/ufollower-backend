from django.contrib import admin

from . import models


@admin.register(models.MyFeed)
class MyFeedAdmin(admin.ModelAdmin):
    pass


@admin.register(models.MyNewEntry)
class MyNewEntryAdmin(admin.ModelAdmin):
    list_display = ['user', 'entry']
    ordering = ('-entry__pub_date', 'entry__title')


@admin.register(models.MySavedEntry)
class MySavedEntryAdmin(admin.ModelAdmin):
    pass


@admin.register(models.MyStarredEntry)
class MyStarredEntryAdmin(admin.ModelAdmin):
    pass


@admin.register(models.MyComment)
class MyCommentAdmin(admin.ModelAdmin):
    pass
