from django.db.models.signals import post_save

from ufollower.apps.feeds.models import Entry

from .models import MyNewEntry


def new_entry_saved(sender, instance, created, **kwargs):
    subscribed_users = instance.feed.myfeed_set.values_list('user', flat=True)
    for user_id in subscribed_users:
        _, _ = MyNewEntry.objects.get_or_create(
            user_id=user_id,
            entry=instance,
        )


post_save.connect(new_entry_saved, sender=Entry, dispatch_uid='new_entry_saved')
