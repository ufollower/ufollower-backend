import os
from datetime import timedelta

from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ufollower.settings.dev')

app = Celery('ufollower')

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()

app.conf.beat_schedule = {
    'Update feeds': {
        'task': 'feeds.update_feeds',
        'schedule': timedelta(minutes=settings.FEEDS_TASK_UPDATE_INTERVAL),
        'options': {
            'expires': 60 * 60,
        }
    },
}
