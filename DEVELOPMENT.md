DEVELOPMENT
===========

Clone this repository from gitlab.com:
```bash
$ git clone git@gitlab.com:ufollower/ufollower-backend.git
$ cd ufollower-backend
```

There are several ways to run the project locally

Usual way
---------

- Prepare and activate virtual environment

```bash
$ python3 -m venv venv
$ source venv/bin/activate
```

- Install development requirements

```bash
$ pip install -r requirements/dev.txt
```

- Create `app.env` file with local settings

```bash
$ cp example.env app.env
```

- Fill the file with appropriate settings

By default, SQLite used as a database storage, but you can overwrite it simply putting
appropriate `DATABASE_URL` inside `app.env`

```
DATABASE_URL=psql://ufollower:password@localhost/ufollower
```

- Migrations

```bash
python manage.py migrate
```

- Run local server

```bash
$ python manage.py runserver
```

- Run tests

```bash
# tests with coverage
$ pytest --cov ufollower
```

Docker way
----------

- Create `compose.env` file with desired parameters

```bash
$ cp example.env compose.env
```

- Fill the file with appropriate settings

```
# compose.env

DJANGO_DEBUG=False
REDIS_URL=redis://redis:6379/1
DATABASE_URL=psql://postgres@postgres:5432/postgres
CELERY_BROKER_URL=redis://redis:6379/2
DJANGO_SETTINGS_MODULE=ufollower.settings.prod
DJANGO_READ_DOT_ENV_FILE=False
DJANGO_SECURE_SSL_REDIRECT=False
DJANGO_SESSION_COOKIE_SECURE=False
```

- Run docker-compose

```bash
$ docker-compose up --build -d
```

- Migrate DB

```bash
$ docker-compose run application django-admin migrate --noinput
```
